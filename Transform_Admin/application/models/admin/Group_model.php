<?php
    class Group_model extends CI_Model{

        public function add_user($data){
            $this->db->insert('ci_users', $data);
            return true;
        }

        public function get_all_groups(){
            
            $this->db->select('*');
            $this->db->from('tb_group');
            $this->db->order_by('reg_date', 'DESC');    
            $query=$this->db->get();

            return $result = $query->result_array();
        }
        
        public function get_group_creator($group_id) {
            
            $result = array();
            $this->db->select('tb_group.*, tb_user.username, tb_user.email');
            $this->db->from('tb_group');        
            $this->db->join('tb_user', 'tb_user.id = tb_group.user_id');
            $this->db->where('tb_group.id', $group_id);
            $query = $this->db->get();
            
            if ($query->num_rows() > 0 ) {
                
                $row = $query->row();
                    
                $point = 0;
                $status = 0;
                $sub_query = $this->db->get_where('tb_point', 
                                                   array('group_id' => $group_id, 
                                                         'user_id' => $row->user_id)
                                                 );
                if ($sub_query->num_rows() > 0) {                    
                    $point = $sub_query->row()->total_point; 
                    $status = 1;                        
                }
                
                $result = array('user_id' => $row->user_id,
                             'total_points' => $point,
                             'user_name' => $row->username,
                             'email' => $row->email,
                             'status' => $status);
                
                
            }
        
            return $result;
            
        }
        
        function get_group_users($group_id) {        
        
            $result = array();
            $this->db->select('tb_invite.*, tb_user.username');
            $this->db->from('tb_invite');        
            $this->db->join('tb_user', 'tb_user.id = tb_invite.user_id');
            $this->db->where('tb_invite.group_id', $group_id);
            $query = $this->db->get();
            
            if ($query->num_rows() > 0 ) {
                
                foreach($query->result() as $row) {
                    
                    $point = 0;
                    $sub_query = $this->db->get_where('tb_point', 
                                                       array('group_id' => $group_id, 
                                                             'user_id' => $row->user_id)
                                                     );
                    if ($sub_query->num_rows() > 0) {
                        
                        $point = $sub_query->row()->total_point;                         
                    }
                    
                    $arr = array('user_id' => $row->user_id,
                                 'total_points' => $point,
                                 'user_name' => $row->username,
                                 'email' => $row->email,
                                 'status' => $row->status);
                    array_push($result, $arr); 
                }
            }
        
            return $result;
        }
         

    }

?>