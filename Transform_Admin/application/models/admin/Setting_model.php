<?php
    class Setting_model extends CI_Model{

        public function get_library_list(){             
            $query = $this->db->get('tb_library');
            return $result = $query->result_array();
        }
        
        public function get_help_list(){             
            $query = $this->db->get('tb_help');
            return $result = $query->result_array();
        }
        
        public function add_new_library($data) {
            
            $this->db->insert('tb_library', $data);            
            return true;             
        }        
        
        public function add_new_help($data) {
            
            $this->db->insert('tb_help', $data);             
            return true;
            
        }
    }

?>