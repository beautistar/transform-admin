<?php
  
  class Api_model extends CI_Model {
      
      function exist_user($username) {
          $this->db->where('username', $username);
          $query = $this->db->get('ci_users');
          if ($query->num_rows() > 0) {
              return $query->row()->id;
          } else {
              return 0;
          }
      }
      
      function register_user($data) {
          
          $this->db->insert('ci_users', $data);
          return $this->db->insert_id();
      }
      
      function update_user($id, $data) {
          
          $this->db->where('id', $id);
          $this->db->update('ci_users', $data);
      }
      
      function exist_event($event_id) {
          
          $this->db->where('event_id', $event_id);
          $query = $this->db->get('ci_event');
          return $query->num_rows();
      }
      function create_event($data) {
          
          $this->db->insert('ci_event', $data);
          return true;          
      }
      
      function login_event($data){
          $query = $this->db->get_where('ci_event', array('event_id' => $data['event_id']));
          if ($query->num_rows() == 0){
            return false;
          }
          else{
            //Compare the password attempt with the password we have stored.
            $result = $query->row_array();
            $validPassword = password_verify($data['event_password'], $result['event_password']);
            if($validPassword){
                return $result = $query->row_array();
            }                
          }
      }
      
      function upload_photo($data) {
        
          $this->db->insert('ci_photo', $data);
          return $this->db->insert_id();
      }
      
      function get_event_photos($event_id) {
          
          $this->db->where('event_id', $event_id);
          $query =  $this->db->get('ci_photo');
          return $result = $query->result_array();
      }
      
      function add_order($data) {
          
          $this->db->insert('ci_order', $data);
          return true;
      }
      
      function get_prices() {
          
          return $this->db->get('ci_price')->row_array();
      }
      
  }
?>
