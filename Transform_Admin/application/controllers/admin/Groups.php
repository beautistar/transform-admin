<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Groups extends MY_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model('admin/group_model', 'group_model');
            $this->load->helper('url');
            $this->load->library('session');
        }

        public function index(){
            $data['all_groups'] =  $this->group_model->get_all_groups();
            $data['title'] = 'Group List';
            $data['view'] = 'admin/groups/group_list';
            $this->load->view('admin/layout', $data);
        }
        
        public function view_detail($id = 0) {
            
            $data['group_members'] =  $this->group_model->get_group_users($id);
            $data['group_creator'] =  $this->group_model->get_group_creator($id);
            $data['title'] = 'Group Detail';
            $data['view'] = 'admin/groups/group_detail';
            $this->load->view('admin/layout', $data);
        }
        
        //---------------------------------------------------------------
        //  Delete Users
        public function del_group($id = 0){
            
            $this->db->delete('tb_group', array('id' => $id));
            $this->db->delete('tb_invite', array('group_id' => $id));
            $this->db->delete('tb_message', array('group_id' => $id));
            $this->db->delete('tb_point', array('group_id' => $id));            
            
            $this->session->set_flashdata('msg', 'Group has been Deleted Successfully!');
            redirect(base_url('admin/groups'));
        } 
    }


?>