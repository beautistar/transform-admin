<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Settings extends MY_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model('admin/setting_model', 'setting_model');
        }

        public function library(){
            
            $data['library_list'] = $this->setting_model->get_library_list();
            $data['title'] = 'Setting';
            $data['view'] = 'admin/settings/library_list';
            $this->load->view('admin/layout', $data);
        }
        
        public function help(){
            
            $data['help_list'] = $this->setting_model->get_help_list();
            $data['title'] = 'Setting';
            $data['view'] = 'admin/settings/help_list';
            $this->load->view('admin/layout', $data);
        }
        
        //-----------------------------------------------------------------\
        // Add Library
        public function add_library(){
            
            if($this->input->post('submit')){

                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('description', 'Description', 'trim|required'); 
                       
                if (empty($_FILES['picture']['name'])) {
                    $this->form_validation->set_rules('picture', 'Picture', 'required');
                }
                
                if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'File', 'required');
                }
                
                if ($this->form_validation->run() == FALSE) {
                    $data['title'] = 'Add New';
                    $data['view'] = 'admin/settings/add_library';                
                    $this->load->view('admin/layout', $data);
                }
                else {
                    
                    if(isset($_FILES['file']) && isset($_FILES['picture'])){
                        $errors = array();
                        
                        $preview_photo = "";
                        $content_file = "";
                        $success = 0;
                        
                        if(!is_dir("uploadfiles/")) {
                            mkdir("uploadfiles/");
                        }
                        $upload_path = "uploadfiles/";  

                        $cur_time = time();

                        $dateY = date("Y", $cur_time);
                        $dateM = date("m", $cur_time);

                        if(!is_dir($upload_path."/".$dateY)){
                            mkdir($upload_path."/".$dateY);
                        }
                        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                            mkdir($upload_path."/".$dateY."/".$dateM);
                        }

                        $upload_path .= $dateY."/".$dateM."/";
                        $upload_url = base_url().$upload_path;
                        
                        $this->load->library('upload');
                        
                        $file_name1 = $_FILES['picture']['name'];
                        $file_size1 =$_FILES['picture']['size'];
                        $file_tmp1 =$_FILES['picture']['tmp_name'];
                        $file_type1=$_FILES['picture']['type']; 
                        $file_ext1 = pathinfo($file_name1, PATHINFO_EXTENSION);
                        $expensions1= array("jpg", "png", "jpeg", "JPG", "PNG", "JPEG"); 
                        
                        $file_name2 = $_FILES['file']['name'];
                        $file_size2 =$_FILES['file']['size'];
                        $file_tmp2 =$_FILES['file']['tmp_name'];
                        $file_type2=$_FILES['file']['type'];
                        $file_ext2 = pathinfo($file_name2, PATHINFO_EXTENSION);
                        $expensions2= array("mp4", "pdf", "mov", "avi", "MOV", "MP4", "PDF", "AVI");                       
                        
                        if(in_array($file_ext1,$expensions1)=== false || in_array($file_ext2,$expensions2)=== false){
                            $message = "File extension not allowed, please choose correct file format.";
                            $errors[]=  $message;
                            $this->session->set_flashdata('msg', $message);
                            $data['title'] = 'Add New';
                            $data['view'] = 'admin/settings/add_library';                
                            $this->load->view('admin/layout', $data);
                            return;
                        }

                        if($file_size2 > 500 * 1024 * 1024){
                            $message =  'File size must be less than 100 MB';
                            $errors[] =  $message;
                            $this->session->set_flashdata('msg', $message);
                            $data['title'] = 'Add New';
                            $data['view'] = 'admin/settings/add_library';                
                            $this->load->view('admin/layout', $data);
                            return;
                        }                        
                        
                        if(empty($errors)==true){
                            // Upload file.                             
                        
                            $w_uploadConfig1 = array(
                                'upload_path' => $upload_path,
                                'upload_url' => $upload_url,
                                'allowed_types' => "png|jpg|jpeg|PNG|JPG|JPEG",
                                'overwrite' => TRUE,
                                'max_size' => "5000000KB",
                                'max_width' => 3000,
                                'max_height' => 4000,
                                'file_name' => 'library_pic_'.date('Y-m-d0hms')
                            );
                            
                            $w_uploadConfig2 = array(
                                'upload_path' => $upload_path,
                                'upload_url' => $upload_url,
                                'allowed_types' => "mp4|pdf|mov|avi|MOV|PDF|AVI|MP4",
                                'overwrite' => TRUE,
                                'max_size' => "5000000KB",
                                'max_width' => 3000,
                                'max_height' => 4000,
                                'file_name' => 'library_file_'.date('Y-m-d0hms')
                            );
                            
                            $this->upload->initialize($w_uploadConfig1);                             
                            
                            if ($this->upload->do_upload('picture')) {                                    

                                $file_url = $w_uploadConfig1['upload_url'].$this->upload->file_name;
                                $preview_photo = $file_url; 
                                $success = 1;                                   
                                
                            } else {
                                
                                $error = $this->upload->display_errors(); 

                                $this->session->set_flashdata('msg', $error);
                                $data['title'] = 'Add new';
                                $data['view'] = 'admin/settings/add_library';                                  
                                $this->load->view('admin/layout', $data);
                            }
                            
                            $this->upload->initialize($w_uploadConfig2);
                                
                            if ($this->upload->do_upload('file')) {                                    

                                $file_url = $w_uploadConfig2['upload_url'].$this->upload->file_name;
                                $content_file = $file_url;
                                $success++;                                    
                                
                            } else {                                
                                
                                $error = $this->upload->display_errors(); 
                                
                                $this->session->set_flashdata('msg', $error);
                                $data['title'] = 'Add new';
                                $data['view'] = 'admin/settings/add_library';                                  
                                $this->load->view('admin/layout', $data);
                            }                            
                        }                        
                        
                        if ($success == 2) {
                            
                            $data = array(                                    
                                    'title' => $this->input->post('title'),
                                    'description' => $this->input->post('description'),                        
                                    'preview_picture' => $preview_photo,                        
                                    'file_url' => $content_file,                        
                                    'created_at' => date('Y-m-d h:m:s'),
                                );
                            $data = $this->security->xss_clean($data);
                                    
                            $result = $this->setting_model->add_new_library($data);
                            if($result){
                                $this->session->set_flashdata('msg', 'New Library has been added Successfully!');
                                redirect(base_url('admin/settings/library'));                     
                            }
                        }                   
                    }    
                }
            }
            else {                
                $data['title'] = 'Add New';
                $data['view'] = 'admin/settings/add_library';                
                $this->load->view('admin/layout', $data);
            }            
        }
        
        
        //-----------------------------------------------------------------\
        // Add Help
        public function add_help(){
            
            if($this->input->post('submit')){

                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('description', 'Description', 'trim|required'); 
                       
                if (empty($_FILES['picture']['name'])) {
                    $this->form_validation->set_rules('picture', 'Preview Picture', 'required');
                }
                
                if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'Content File', 'required');
                }
                
                if ($this->form_validation->run() == FALSE) {
                    $data['title'] = 'Add New';
                    $data['view'] = 'admin/settings/add_help';                
                    $this->load->view('admin/layout', $data);
                    
                    return;
                }
                else {
                    
                    if(isset($_FILES['file']) && isset($_FILES['picture'])){
                        $errors = array();
                        
                        $preview_photo = "";
                        $content_file = "";
                        $success = 0;
                        
                        if(!is_dir("uploadfiles/")) {
                            mkdir("uploadfiles/");
                        }
                        $upload_path = "uploadfiles/";  

                        $cur_time = time();

                        $dateY = date("Y", $cur_time);
                        $dateM = date("m", $cur_time);

                        if(!is_dir($upload_path."/".$dateY)){
                            mkdir($upload_path."/".$dateY);
                        }
                        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                            mkdir($upload_path."/".$dateY."/".$dateM);
                        }

                        $upload_path .= $dateY."/".$dateM."/";
                        $upload_url = base_url().$upload_path;
                        
                        $this->load->library('upload');
                        
                        $file_name1 = $_FILES['picture']['name'];
                        $file_size1 =$_FILES['picture']['size'];
                        $file_ext1 = pathinfo($file_name1, PATHINFO_EXTENSION);
                        $expensions1= array("jpg", "png", "jpeg", "JPG", "PNG", "JPEG"); 
                        
                        $file_name2 = $_FILES['file']['name'];
                        $file_size2 =$_FILES['file']['size'];                        
                        $file_ext2 = pathinfo($file_name2, PATHINFO_EXTENSION);
                        $expensions2= array("mp4", "pdf", "mov", "avi", "MOV", "MP4", "PDF", "AVI");                       
                        
                        if(in_array($file_ext1, $expensions1)=== false || in_array($file_ext2, $expensions2) === false){
                            $message =  "File extension not allowed, please choose correct file format.";
                            $errors[]=  $message;
                            $this->session->set_flashdata('msg', $message);
                            $data['title'] = 'Add New';
                            $data['view'] = 'admin/settings/add_help';                
                            $this->load->view('admin/layout', $data);
                            return;
                        }

                        if($file_size2 > 500 * 1024 * 1024){
                            $message =  'File size must be less than 100 MB';
                            $errors[] = $message;
                            $this->session->set_flashdata('msg', $message);
                            $data['title'] = 'Add New';
                            $data['view'] = 'admin/settings/add_help';                
                            $this->load->view('admin/layout', $data);
                            return;
                        }                        
                        
                        if(empty($errors)==true){
                            // Upload file.                             
                        
                            $w_uploadConfig1 = array(
                                'upload_path' => $upload_path,
                                'upload_url' => $upload_url,
                                'allowed_types' => "png|jpg|jpeg|JPEG|PNG|JPG",
                                'overwrite' => TRUE,
                                'max_size' => "5000000KB",
                                'max_width' => 3000,
                                'max_height' => 4000,
                                'file_name' => 'help_pic_'.date('Y-m-d0hms')
                            );
                            
                            $w_uploadConfig2 = array(
                                'upload_path' => $upload_path,
                                'upload_url' => $upload_url,
                                'allowed_types' => "mp4|pdf|mov|avi|MP4|PDF|MOV|AVI",
                                'overwrite' => TRUE,
                                'max_size' => "5000000KB",
                                'max_width' => 3000,
                                'max_height' => 4000,
                                'file_name' => 'help_file_'.date('Y-m-d0hms')
                            );
                            
                            $this->upload->initialize($w_uploadConfig1);                             
                            
                            if ($this->upload->do_upload('picture')) {                                    

                                $file_url = $w_uploadConfig1['upload_url'].$this->upload->file_name;
                                $preview_photo = $file_url; 
                                $success = 1;                                   
                                
                            } else {
                                
                                $error = $this->upload->display_errors(); 

                                $this->session->set_flashdata('msg', $error);
                                $data['title'] = 'Add new';
                                $data['view'] = 'admin/settings/add_help';                                  
                                $this->load->view('admin/layout', $data);
                            }
                            
                            $this->upload->initialize($w_uploadConfig2);
                                
                            if ($this->upload->do_upload('file')) {                                    

                                $file_url = $w_uploadConfig2['upload_url'].$this->upload->file_name;
                                $content_file = $file_url;
                                $success++;                                    
                                
                            } else {                                
                                
                                $error = $this->upload->display_errors(); 
                                
                                $this->session->set_flashdata('msg', $error);
                                $data['title'] = 'Add new';
                                $data['view'] = 'admin/settings/add_help';                                  
                                $this->load->view('admin/layout', $data);
                            }                            
                        }                        
                        
                        if ($success == 2) {
                            
                            $data = array(                                    
                                    'title' => $this->input->post('title'),
                                    'description' => $this->input->post('description'),                        
                                    'preview_picture' => $preview_photo,                        
                                    'file_url' => $content_file,                        
                                    'created_at' => date('Y-m-d h:m:s'),
                                );
                            $data = $this->security->xss_clean($data);
                                    
                            $result = $this->setting_model->add_new_help($data);
                            if($result){
                                $this->session->set_flashdata('msg', 'New has been added Successfully!');
                                redirect(base_url('admin/settings/help'));                     
                            }
                        }                   
                    }    
                }
            }
            else {                
                $data['title'] = 'Add New';
                $data['view'] = 'admin/settings/add_help';                
                $this->load->view('admin/layout', $data);
            } 
            
        }
        
        //---------------------------------------------------------------
        //  Delete Library
        public function del_library($id = 0){
            $this->db->delete('tb_library', array('id' => $id));
            $this->session->set_flashdata('msg', 'It has been Deleted Successfully!');
            redirect(base_url('admin/settings/library'));
        }
        
        //---------------------------------------------------------------
        //  Delete Help
        public function del_help($id = 0){
            $this->db->delete('tb_help', array('id' => $id));
            $this->session->set_flashdata('msg', 'It has been Deleted Successfully!');
            redirect(base_url('admin/settings/help'));
        }
                
    }


?>