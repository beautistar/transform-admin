<link rel="stylesheet" href="<?= base_url() ?>public/custom_css/upload_button.css">
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h4><i class="fa fa-plus"></i> &nbsp; Add New</h4>
        </div>        
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open_multipart(base_url('admin/settings/add_help/'), 'class="form-horizontal"');  ?> 
              
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title</label>

                <div class="col-sm-9">
                  <input type="text" name="title" class="form-control" id="title" placeholder="">
                </div>
              </div>
              
              <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Description</label>

                <div class="col-sm-9">                  
                  <textarea name="description" id="description" cols="50" rows="5" class="form-control"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="file" class="col-sm-2 control-label">Preview picture</label>
                <div class="col-sm-9" align="bottom">                     
                  <input type="file" name="picture" id="file" > (.png, jpg, jpeg image file)
                </div>
              </div>
              
              <div class="form-group">
                <label for="file" class="col-sm-2 control-label">Content file</label>
                <div class="col-sm-9" align="bottom">                     
                  <input type="file" name="file" id="file" > (.mp4, .pdf, .mov, .avi file max size is 100MB)
                </div>
              </div>               
  
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Submit" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 