<!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css"> 
  

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h3><i class="fa fa-book"></i> &nbsp; Library</h3>
        </div>
        <div class="col-md-6 text-right">
          <div class="btn-group margin-bottom-20"> 
            <a href="<?= base_url('admin/settings/add_library'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>            
          </div>
        </div>
        
      </div>
    </div>
  </div>
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
          <th>No.</th>          
          <th>Title</th>          
          <th>Description</th>
          <th>Preview picture</th>
          <th>File</th>
          <th>Created Date</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
          <?php $i=0; foreach($library_list as $row): ?>
          <tr>
            <td><?= ++$i;?></td>
            <td><?= $row['title'];?></td>
            <td><?= $row['description'];?></td>            
            <td><a href="<?= $row['preview_picture'];?>" target="_blank">See file</a></td>            
            <td><a href="<?= $row['file_url'];?>" target="_blank">See file</a></td>            
            <td><?= $row['created_at'];?></td>           
            
            <td>                
              <a data-href="<?= base_url('admin/settings/del_library/'.$row['id']); ?>" class="btn btn-danger btn-flat btn-sm" data-toggle="modal" data-target="#confirm-delete" >Delete</a>              
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script> 

  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>
  
<script>
$("#view_library").addClass('active');
</script>   