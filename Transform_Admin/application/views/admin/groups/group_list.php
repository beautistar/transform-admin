<!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css"> 
  

 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body">
        <div class="col-md-6">
          <h3><i class="fa fa-list"></i> &nbsp; Group List</h3>
        </div>
      </div>
    </div>
  </div>
   <div class="box border-top-solid">
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
          <th>No.</th>          
          <th>Group Name</th>          
          <th>Group Type</th>
          <th>Price</th>
          <th>Start Date</th>    
          <th>End Date</th>
          <th>Creator Name</th>
          <th>Created Date</th>          
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
          <?php $i=0; foreach($all_groups as $row): ?>
          <tr>
            <td><?= ++$i;?></td>
            <td><?= $row['group_name'];?></td>
            <td><?= $row['group_type'];?></td>            
            <td><?= $row['price'];?></td>            
            <td><?= $row['start_date'];?></td>            
            <td><?= $row['end_date'];?></td>            
            <td><?= $row['creator_name'];?></td>            
            <td><?= $row['reg_date']; ?></td>            
            <td>                
              <a href="<?= base_url('admin/groups/view_detail/'.$row['id']); ?>" class="btn btn-primary btn-flat btn-sm">Detail</a>              
              <a data-href="<?= base_url('admin/groups/del_group/'.$row['id']); ?>" class="btn btn-danger btn-flat btn-sm" data-toggle="modal" data-target="#confirm-delete">Delete</a>              
            
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  


<!-- Modal -->
<div id="confirm-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Dialog</h4>
      </div>
      <div class="modal-body">
        <p>As you sure you want to delete this group?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>

  </div>
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script> 
  <script type="text/javascript">
      $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
  </script>
  
<script>
$("#view_groups").addClass('active');
</script>   