<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Group Creator : </h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box border-top-solid">
          <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th>Creator Name</th>          
                  <th>Creator Email</th>
                  <th>Paid</th>
                  <th>Points</th>   

                </tr>
                </thead>
                <tbody>                   
                  <tr>
                    <td><?= $group_creator['user_name'];?></td>
                    <td><?= $group_creator['email'];?></td>            
                    <td><?php if ($group_creator['status'] == 1) echo "Yes"; else echo "No"; ?></td>            
                    <td><?= $group_creator['total_points'];?></td>                    
                  </tr>                  
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
    </div>
  </div>  

</section>


<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Group members : </h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box border-top-solid">
          <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th>No.</th>          
                  <th>User Name</th>          
                  <th>Email</th>
                  <th>Paid</th>
                  <th>Points</th>   

                </tr>
                </thead>
                <tbody>
                  <?php $i=0; foreach($group_members as $row): ?>
                  
                  <tr>
                    <td><?= ++$i;?></td>
                    <td><?= $row['user_name'];?></td>
                    <td><?= $row['email'];?></td>            
                    <td><?php if ($row['status'] == 1) echo "Yes"; else echo "No"; ?></td>            
                    <td><?= $row['total_points'];?></td>
                    
                  </tr>
                  <?php endforeach; ?>
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section>
